using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

public class CqueueTests
{



    [Fact]
    public void TestName()
    {
        //Given

        //When

        //Then

        Assert.True(true);
    }

    [Fact]
    public void WhenQueueisCreated_returntrue()
    {
        //Arrange


        //Act
        var tq = new Cqueue();

        //Assert

        Assert.NotNull(tq);
    }

    [Fact]
    public void WhenEnqueued_lengthIncreasesByNumberOfItemsEnqueued()
    {
        //Given
        var tq = new Cqueue();
        int i = 4;
        int j = 5;
        int k = 7;
        //When
        tq.enqueue(i);
        tq.enqueue(j);
        tq.enqueue(k);
        var len = tq.getlength();

        //Then
        Assert.True(len == 3);
    }

    [Fact]
    public void WhenDqeueued_lengthDecreasesByNumberOfItemsDequeued()
    {
        //Given
        var tq = new Cqueue();
        int i = 4;
        int j = 5;
        int k = 7;
        //When
        tq.enqueue(i);
        tq.enqueue(j);
        tq.enqueue(k);
        var firstItem = tq.dequeue();
        var len = tq.getlength();


        //Then
        Assert.True(len == 2);
        Assert.Equal(firstItem, i);
    }

    [Theory]
    [InlineData(1, 4)]
    [InlineData(2, 5)]
    [InlineData(3, 7)]
    public void WhenSearch_returnIndexOfSpecifiedItem(int expectedIndex, int inputItem)
    {
        //Given
        var tq = new Cqueue();
        int i = 4;
        int j = 5;
        int k = 7;
        //When
        tq.enqueue(i);
        tq.enqueue(j);
        tq.enqueue(k);
        var indexOfItem = tq.find(inputItem);
        var indexOfItem2 = tq.find(2);


        //Then
        Assert.Equal(indexOfItem, expectedIndex);
        //Assert.Equal(indexOfItem2, -1);

    }



}