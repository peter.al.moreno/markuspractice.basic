using System;
using System.Linq;
using Xunit;

public class MSetTests
{


    [Fact]
    public void TestName()
    {
        //Given (assign)

        //When (act)

        //Then (assert)
        Assert.Equal(4, Program.Add(2, 2));
    }


    [Fact]
    public void CreateClass()
    {
        //arrange
        //act
        //assert


        var TSet = new mSet();

        Assert.NotNull(TSet);
    }

    [Fact]
    public void EmptySet()
    {

        mSet emptyset = new mSet();

        Assert.True(emptyset.Isempty());

    }
}